# Instalación
---

1. Clonar el repositorio.
2. `composer install`.
3. `php artisan key:generate`.
4. `php artisan migrate`.
5. `npm install`.
6. `npm run dev`.

## Variables de entorno

El archivo `.env` debe contener las siguientes variables:

```php
REDIS_CLIENT=predis
QUEUE_DRIVER=redis
REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=null
---
BROADCAST_DRIVER=redis
LARAVEL_ECHO_HOST=echo-server.test
LARAVEL_ECHO_PORT=6001
```

## Laravel Echo Server

### Manualmente

El archivo `laravel-echo-server.json` contiene la configuración necesaria para ejecutar el servidor de laravel echo para escucar los mensajes.

Ejecutar: `laravel-echo-server start` para iniciar el servidor.


### Docker

Verificar que el `docker-compose.yml` contenga el servicio de `echo-server`.

Tener en la carpeta del proyecto el archivo `laravel-echo-server.json` con la configuración necesaria.


## Redis Server

Se hace uso de las colas de redis para poder gestionar el manejo de los mensajes de manera persistente. Para utilizarlo junto con el proyecto se deben tener en cuenta las variasbles de entorno mencionadas. Si se necesita ajustar algo respecto a este servicio, se debe revisar el archivo `config/database.php`.

### Manualmente

Ejecutar: `php artisan queue:work` para poner a trabajar las colas y escuchar las conexiones de los clientes.

### Docker

Verificar que el `docker-compose.yml` contenga el servicio de `redis` y este corriendo.

Adicionalmente, se necesita del servicio de `supervisor` que se encarga de iniciar las colas de trabajo cada que se carga el proyecto.
