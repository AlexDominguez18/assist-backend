# PHP
FROM php:8.1-fpm

# Copy composer.lock and composer.json
COPY ./composer.lock ./composer.json /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    --no-install-recommends libgmp-dev \
    nano \
    ssh-client \
    gnupg2 \
    libxml2-dev \
    build-essential cmake \
    default-mysql-client \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    libonig-dev \
    supervisor

ENV LANG=C.UTF-8
# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/* && curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install nodejs -y

# Install extensions
RUN docker-php-ext-install -j$(nproc) gmp
RUN docker-php-ext-install pdo_mysql mysqli mbstring zip exif pcntl soap
RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
RUN docker-php-ext-install gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# -------------------------------------------------------------------------

# Add user for laravel application
RUN groupadd -g 1000 www
RUN groupadd -g 1002 dev-team
RUN useradd -u 1000 -ms /bin/bash -g www www
RUN usermod -aG dev-team www

# Copy existing application directory contents
COPY . /var/www

# Add queue worker conf
ADD --chown=www:www ./queue-worker.conf /etc/supervisor/conf.d/

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

#Permissions to supervisor
RUN chown -R www:www /var/log/supervisor
RUN chown -R www:www /etc/supervisor 
RUN chown -R www:www /var/run/

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
EXPOSE 6001

CMD ["php-fpm"]