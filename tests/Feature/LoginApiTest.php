<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if the email and passwords match with a user in database.
     *
     * @return void
     */
    public function testIncorrectData()
    {
        User::factory()->create();
        
        $userData = [
            'email' => 'alex@email.com',
            'password' => '1234567890' 
        ];

        $this->json('POST', 'api/login', $userData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                "message" => "Correo y/o contraseña inválidas"
            ]);
    }

    /**
     * Test if login is successful
     * 
     * @return void
     */
    public function testSuccessfulLogin()
    {
        User::factory()->create([
            'email' => 'alex@email.com',
            'password' => Hash::make('1234567890')
        ]);

        $userData = [
            'email' => 'alex@email.com',
            'password' => '1234567890'
        ];

        $this->json('POST', 'api/login', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type'
            ]);
    }
}
