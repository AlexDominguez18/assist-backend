<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if the UserRegisterRequest throws the error fields.
     *
     * @return void
     */
    public function testRequiredFieldsRegistration()
    {
        $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "Se necesita un nombre (and 2 more errors)",
                "errors" => [
                    "name" => [
                        "Se necesita un nombre"
                    ],
                    "email" => [
                        "Se necesita un correo"
                    ],
                    "password" => [
                        "Se necesita una contraseña"
                    ]
                ]
            ]);
    }

    /**
     * Test if the password_confirmation is set.
     * 
     * @return void
     */
    public function testConfirmationPassword()
    {
        $userData = [
            'name' => 'Alex Duran',
            'email' => 'alex@email.com',
            'password' => '1234567890'
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "No se ha confirmado la contraseña",
                "errors" => [
                    "password" => [
                        "No se ha confirmado la contraseña"
                    ]
                ]
            ]);
    }

    /**
     * Test if register process is successful.
     * 
     * @return void
     */
    public function testSuccessfulRegister()
    {
        $userData = [
            'name' => 'Alex Duran',
            'email' => 'alex@email.com',
            'password' => '1234567890',
            'password_confirmation' => '1234567890'
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'token_type'
            ]);
    }
}
