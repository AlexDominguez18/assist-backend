<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' =>'Se necesita un nombre',
            'name.min' => 'Nombre muy corto',
            'name.string' => 'Solo se permiten letras',
            'name.max' => 'Nombre demasiado largo',
            'email.required' => 'Se necesita un correo',
            'email.email' => 'Formato de correo inválido',
            'email.unique' => 'Ya existe una cuenta asociada a este correo',
            'password.required' => 'Se necesita una contraseña',
            'password.min' => 'Debe tener al menos 8 carácteres',
            'password.confirmed' => 'No se ha confirmado la contraseña'
        ];
    }
}
