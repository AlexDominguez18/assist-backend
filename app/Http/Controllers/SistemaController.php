<?php

namespace App\Http\Controllers;

use App\Models\Sistema;
use Illuminate\Http\Request;

class SistemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sistemas = Sistema::all();
        return view('sistemas.index-sistema', compact('sistemas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sistemas.form-sistema');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string|min:1|max:255',
            'url' => ['required', 'string', 'min:10', 'max:255', 'regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/i'],
        ]);

        $sistema = Sistema::create([
            'nombre' => $request->nombre,
            'url' => $request->url,
        ]);

        return redirect()->route('sistemas.index')
        ->with([
            'message' => 'Sistema registrado con éxito',
            'alert-class' => 'alert-success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sistema  $sistema
     * @return \Illuminate\Http\Response
     */
    public function show(Sistema $sistema)
    {
        return view('sistemas.show-sistema', compact('sistema'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sistema  $sistema
     * @return \Illuminate\Http\Response
     */
    public function edit(Sistema $sistema)
    {
        return view('sistemas.form-sistema',compact('sistema'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sistema  $sistema
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sistema $sistema)
    {

        $this->validate($request, [
            'nombre' => 'required|string|min:1|max:255',
            'url' => ['required', 'string', 'min:10', 'max:255', 'regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/i'],
        ]);

        $sistema->nombre = $request->nombre;
        $sistema->url = $request->url;

        $sistema->update();

        return redirect()->route('sistemas.show', $sistema->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sistema  $sistema
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sistema = Sistema::findOrFail($id);

        $sistema->delete();
        return redirect()->route('sistemas.index');
    }
}
