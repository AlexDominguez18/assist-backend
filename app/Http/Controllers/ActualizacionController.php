<?php

namespace App\Http\Controllers;

use App\Models\Actualizacion;
use App\Models\Sistema;
use App\Models\Rol;
use Illuminate\Http\Request;

class ActualizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actualizaciones = Actualizacion::all();
        return view('actualizaciones.index-actualizacion', compact('actualizaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sistemas = Sistema::all()->pluck('nombre', 'id');
        $roles = Rol::all()->pluck('NombreRolNombreSistema', 'id');
        return view('actualizaciones.form-actualizacion',compact('sistemas','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'descripcion' => 'required|max:1000',
            'sistemas' => 'required',
            'roles' => 'nullable',
        ]);

        $actualizacion = new Actualizacion();
        $actualizacion->titulo = $request->titulo;
        $actualizacion->descripcion = $request->descripcion;
        $actualizacion->save();

        $actualizacion->roles()->attach($request->roles);
        $actualizacion->sistemas()->attach($request->sistemas);


        return redirect()->route('actualizaciones.show', $actualizacion->id)
        ->with([
            'message' => 'Actualizacion registrada con éxito',
            'alert-class' => 'alert-success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actualizacion  $actualizacion
     * @return \Illuminate\Http\Response
     */
    public function show(Actualizacion $actualizacion)
    {
        return view('actualizaciones.show-actualizacion',compact('actualizacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actualizacion  $actualizacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Actualizacion $actualizacion)
    {
        $sistemas = Sistema::all()->pluck('nombre', 'id');
        $roles = Rol::all()->pluck('NombreRolNombreSistema', 'id');
        return view('actualizaciones.form-actualizacion',compact('actualizacion','sistemas','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actualizacion  $actualizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actualizacion $actualizacion)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'descripcion' => 'required|max:1000',
            'sistemas' => 'required',
            'roles' => 'nullable',
        ]);

        $actualizacion->titulo = $request->titulo;
        $actualizacion->descripcion = $request->descripcion;
        $actualizacion->save();

        $actualizacion->roles()->sync($request->roles);
        $actualizacion->sistemas()->sync($request->sistemas);


        return redirect()->route('actualizaciones.show', $actualizacion->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actualizacion  $actualizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actualizacion $actualizacion)
    {
        $actualizacion->delete();
        return redirect()->route('actualizaciones.index');
    }
}

