<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function form(Request $request)
    {
        if ($request->isMethod('post')) {
            dd($request->all());
        }
        return view('ejemplos.form');
    }
    public function rol()
    {
        return view('formRol');
    }
    public function example()
    {
        return view('example');
    }

    public function indexEjemplo()
    {
        return view('indexEjemplo');
    }

    public function show()
    {
        return view('showEjemplo');
    }
}
