<?php

namespace App\Http\Controllers;

use App\Models\Sistema;
use App\Models\Rol;
use Illuminate\Http\Request;

class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Rol::all();
        return view('roles.index-rol', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sistemas = Sistema::all()->pluck('nombre', 'id');
        return view('roles.form-rol',compact('sistemas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:255',
            'sistema' => 'required',
        ]);


        $rol = new Rol();
        $rol->nombre = $request->nombre;
        $rol->sistema_id = $request->sistema;

        $rol->save();

        return redirect()->route('roles.show', $rol->id)
        ->with([
            'message' => 'Rol registrado con éxito',
            'alert-class' => 'alert-success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Rol $rol)
    {
        return view('roles.show-rol',compact('rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function edit(Rol $rol)
    {
        $sistemas = Sistema::all()->pluck('nombre', 'id');
        return view('roles.form-rol',compact('rol','sistemas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rol $rol)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:255',
            'sistema' => 'required',
        ]);

        $rol->nombre = $request->nombre;
        $rol->sistema_id = $request->sistema;

        $rol->save();

        return redirect()->route('roles.show', $rol->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rol $rol)
    {
        $rol->delete();
        return redirect()->route('roles.index');
    }
}

