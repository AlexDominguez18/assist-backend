<?php

namespace App\Http\Controllers;

use App\Models\ChatInvitation;
use Illuminate\Http\Request;

class ChatInvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChatInvitation  $chatInvitation
     * @return \Illuminate\Http\Response
     */
    public function show(ChatInvitation $chatInvitation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChatInvitation  $chatInvitation
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatInvitation $chatInvitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChatInvitation  $chatInvitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatInvitation $chatInvitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChatInvitation  $chatInvitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatInvitation $chatInvitation)
    {
        //
    }
}
