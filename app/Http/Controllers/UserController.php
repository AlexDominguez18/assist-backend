<?php

namespace App\Http\Controllers;

use App\Models\Permiso;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index-user', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Permisos = Permiso::all()->pluck('nombre', 'id');
        return view('users.form-user', compact('Permisos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->tipo == 'Importado'){
            $password = md5(uniqid(rand(), true));
            $request->merge([
                'cargo' => '',
                'permiso' => '3',
                'password' =>  $password,
                'password_confirmation' =>  $password,
            ]);
        }

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'cargo' => 'string|max:255|nullable',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'cargo' => $request->cargo,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $user->cargo = $request->cargo;
        $user->save(); 

        $user->permisos()->attach($request->permisos);

        return redirect()->route('users.show', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show-user', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $Permisos = Permiso::all()->pluck('nombre', 'id');
        return view('users.form-user', compact('user','Permisos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'cargo' => 'nullable|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'. $user->id ,
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        $user->name = $request->name;
        $user->cargo = $request->cargo;
        $user->email = $request->email;

        if($request->password)
            $user->password = Hash::make($request->password);

        $user->update();

        $user->permisos()->sync($request->permisos);

        return redirect()->route('users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index');
    }

}
