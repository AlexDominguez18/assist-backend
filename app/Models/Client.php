<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'name',
        'email',
    ];

    public function system()
    {
        return $this->hasOne(System::class);
    }

    public function chatToken()
    {
        return $this->hasOne(ChatToken::class);
    }

    public function chat()
    {
        return $this->hasOne(Chat::class);
    }

    public function messages()
    {
        return $this->morphMany(Message::class, 'owner');
    }
}
