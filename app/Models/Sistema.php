<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sistema extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = [
        'nombre', 'url'
    ];

    public function roles(){
        return $this->hasMany('App\Models\Rol');
    }

    public function actualizaciones()
    {
        return $this->belongsToMany('App\Models\Actualizacion')
            ->withPivot(['fecha_publicacion']);
    }

}
