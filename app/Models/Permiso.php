<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    use HasFactory;

    protected $fillable = ['nombre'];

    /**Relaciona un permiso con los usuarios que lo poseen */
    public function usuarios()
    {
        return $this->belongsToMany(User::class);
    }

}
