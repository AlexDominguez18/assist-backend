<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actualizacion extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'actualizaciones';

    public function sistemas()
    {
        return $this->belongsToMany('App\Models\Sistema')
            ->withPivot(['fecha_publicacion']);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Rol');
    }
}
