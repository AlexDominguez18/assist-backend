<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'token'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
