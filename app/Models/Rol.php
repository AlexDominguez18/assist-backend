<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $fillable = [
        'nombre', 'sistema_id'
    ];

    protected $table = 'roles';

    public function sistema()
    {
        return $this->belongsTo('App\Models\Sistema');
    }

    public function actualizaciones()
    {
        return $this->belongsToMany('App\Models\Actualizacion');
    }

    public function getNombreRolNombreSistemaAttribute()
    {
        return "{$this->nombre} - {$this->sistema()->get()[0]->nombre}";
    }

}
