<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatInvitation extends Model
{
    use HasFactory;

    protected $fillable = [
        'chat_id',
        'from_user_id',
        'to_user_id',
        'status',
        'content'
    ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function from()
    {
        return $this->belongsTo(User::class);
    }

    public function to()
    {
        return $this->belongsTo(User::class);
    }
}
