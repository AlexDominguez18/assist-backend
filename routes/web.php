<?php

use App\Events\PublicMessage;
use App\Http\Controllers\ActualizacionController;
use App\Http\Controllers\PermisoController;
use App\Http\Controllers\RolController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SistemaController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::match(['get', 'post'],'/form', [App\Http\Controllers\HomeController::class, 'form'])->name('form');
Route::get('/listado', [App\Http\Controllers\HomeController::class, 'indexEjemplo'])->name('indexEjemplo');
Route::get('/show', [App\Http\Controllers\HomeController::class, 'show'])->name('show');

Route::resource('/sistemas', SistemaController::class)->middleware('auth');
Route::resource('/roles', RolController::class)->parameters(['roles' => 'rol'])->middleware('auth');
Route::resource('/actualizaciones', ActualizacionController::class)->parameters(['actualizaciones' => 'actualizacion'])->middleware('auth');
Route::resource('/permisos', PermisoController::class)->middleware('auth');
Route::resource('/users', UserController::class)->middleware('auth');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
