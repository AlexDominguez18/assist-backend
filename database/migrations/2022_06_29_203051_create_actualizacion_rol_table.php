<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actualizacion_rol', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('actualizacion_id');
            $table->unsignedBigInteger('rol_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('actualizacion_id')->references('id')->on('actualizaciones');
            $table->foreign('rol_id')->references('id')->on('roles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actualizacion_rol');
    }
};
