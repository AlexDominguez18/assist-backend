<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actualizacion_sistema', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('actualizacion_id');
            $table->unsignedBigInteger('sistema_id');
            $table->date('fecha_publicacion')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('actualizacion_id')->references('id')->on('actualizaciones');
            $table->foreign('sistema_id')->references('id')->on('sistemas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actualizacion_sistema');
    }
};
