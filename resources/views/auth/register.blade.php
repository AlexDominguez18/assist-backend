@extends('layouts.app')

@section('content')

<div class="form-group">
    <div class="ibox-title">
            <h5>Registro</h5>
    </div>
    <div class="ibox-content">
            {!! Form::open(['route' => 'register', 'method' => 'post'], ['class' => 'form-horizontal']) !!}
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('name', 'Nombre', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-user"></i>
                        </span>
                    </div>
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nombre']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('cargo', 'Cargo', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-stamp"></i>
                        </span>
                    </div>
                    {!! Form::text('cargo', null, ['class' => 'form-control', 'id' => 'cargo', 'placeholder' => 'Cargo']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('email', 'Correo', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-email"></i>
                        </span>
                    </div>
                    {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Correo']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('password', 'Contraseña', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-lock"></i>
                        </span>
                    </div>
                    {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('password', 'Confirmar Contraseña', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-lock"></i>
                        </span>
                    </div>
                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation']) !!}
                </div>
            </div>
            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection
