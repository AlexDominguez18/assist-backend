@extends('layouts.app')

@section('content')
<div class="form-group">
    <div class="ibox-title">
            <h5>Iniciar sesion</h5>
    </div>
    <div class="ibox-content">
            {!! Form::open(['route' => 'login', 'method' => 'post'], ['class' => 'form-horizontal']) !!}
            
            
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('email', 'Correo', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-email"></i>
                        </span>
                    </div>
                    {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('password', 'Contraseña', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-lock"></i>
                        </span>
                    </div>
                    {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                </div>
            </div>
            
            {!! Form::button('Entrar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection
