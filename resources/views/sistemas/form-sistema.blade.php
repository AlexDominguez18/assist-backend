@extends('layouts.app')

@section('content')

<div class="form-group">
    <div class="ibox-title">
        @if(isset($sistema))
            <h5>Actualizar Sistema</h5>
        @else
            <h5>Nuevo Sistema</h5>
        @endisset
    </div>
    <div class="ibox-content">
        @if(isset($sistema))
            {!! Form::model($sistema, ['route' => ['sistemas.update', $sistema->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
        @else
            {!! Form::open(['route' => 'sistemas.store'], ['class' => 'form-horizontal']) !!}
        @endif
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-desktop"></i>
                        </span>
                    </div>
                    {!! Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'Nombre']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-link"></i>
                        </span>
                    </div>
                    {!! Form::text('url', null, ['class' => 'form-control', 'id'=> 'url', 'placeholder' => 'URL']) !!}
                </div>
            </div>

            

            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection