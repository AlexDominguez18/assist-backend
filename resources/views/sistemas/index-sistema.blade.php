@extends('layouts.app')

@section('content')
<div class="row pb-3">
    <div class="col-md-12">
        <a href="{{ route('sistemas.create') }}" class="btn btn-sm btn-info">Nuevo Sistema</a>
    </div>
</div>

<div class="row">
    <div class="col-md-8 offset-sm-2">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Sistemas</h4>
                <h6 class="card-subtitle">listado de sistemas</h6>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Url</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($sistemas as $sistema)
                                <tr>
                                    <td class="text-center"><a href="{{ route('sistemas.show', $sistema->id) }}">{{ $sistema->nombre }}</a></td>
                                    <td>{{ $sistema->url }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush