@extends('layouts.app')

@section('content')
<a href="{{ route('sistemas.index') }}" class="btn btn-sm btn-info">Listado de sistemas</a>
            <div class="row">
                <div class="col-md-8 offset-sm-2">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Sistema</h4>
                            <h6 class="card-subtitle">Descripcion sistema</h6>
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>URL</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$sistema->nombre}}</td>
                                            <td>{{$sistema->url}}</td>
                                            <td>
                                            <div class="text-nowrap">
                                                <div style="">
                                                    {{-- <a href="{{ route('sistemas.edit', $sistema->id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a> --}}
                                                    {!! Form::open(['route' => ['sistemas.destroy', $sistema->id], 'method' => 'delete']) !!}
                                                    <a href="{{ route('sistemas.edit', $sistema->id) }}" data-toggle="tooltip" data-original-title="Editar"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>
                                                    <button type="submit" class="btn" title="Eliminar">
                                                    <i class="fas fa-window-close text-danger"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                                <div style="margin-left: 60px">
                                                    
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@stop

@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush
