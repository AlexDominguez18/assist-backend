<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Echo Test</title>
</head>
<body>
    <div class="container">
        <h1>Laravel Echo Test</h1>
        <div id="broadcast"></div>
    </div>
    
    {{-- <script>
        window.laravelEchoPort = {{ env('LARAVEL_ECHO_PORT') }};
        window.laravelEchoHost = "{{ env('LARAVEL_ECHO_HOST') }}";
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        const userId = '{{ auth()->id() }}';
        window.Echo.channel('public-message-channel')
            .listen('.MessageEvent', (data) => {
                console.log('Pulblic message', data); 
                document.querySelector('#broadcast').innerHTML += `<p>${data.message}</p>`;
            });
    </script> --}}
</body>
</html>