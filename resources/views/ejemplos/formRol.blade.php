@extends('layouts.app')

@section('content')
<div class="card-body">
    <form action="#" class="form-horizontal form-bordered">
        <div class="form-body">
            <div class="form-group row">
                <label class="control-label text-right col-md-3">Rol</label>
                <div class="col-md-9">
                    <input type="text" placeholder="Escriba el rol" class="form-control">
                    <small class="form-control-feedback"> This is inline help </small> </div>
            </div>
            <div class="form-group row">
                <label class="control-label text-right col-md-3">Sistemas</label>
                <div class="col-md-9">
                    <select class="form-control custom-select">
                        <option value="">Ejemplo 1</option>
                        <option value="">Ejemplo 2</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="offset-sm-3 col-md-9">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                            <button type="button" class="btn btn-inverse">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection