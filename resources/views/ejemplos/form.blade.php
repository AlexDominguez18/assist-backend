@extends('layouts.app')

@section('content')
    <div class="row">
       <div class="col-md-6 offset-sm-3">
            <div class="card card-body">
                <h3 class="box-title m-b-0">Título Formulario</h3>
                <p class="text-muted m-b-30 font-13">Subtitulo</p>
                <div class="row">
                    <div class="col-sm-12 colxs-12">
                        {!! Form::open(['route' => 'form', 'class' => 'form']) !!}
                            <div class="form-group">
                                <label for="sistema">label Sistema select</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="mdi mdi-playlist-check"></i>
                                        </span>
                                    </div>
                                    {!! Form::select('sistema', ['' => '', '1' => 'Uno', '2' => 'Dos', '3' => 'Tres'], null, ['class' => 'form-control custom-select', 'id' => 'sistemaSel']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="url"> label URL</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="ti-link"></i>
                                        </span>
                                    </div>
                                    {!! Form::text('url', null, ['class' => 'form-control', 'id'=> 'url', 'placeholder' => 'URL con Icono personalizado']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input">label de input con icono genérico</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="mdi mdi-note-text"></i>
                                        </span>
                                    </div>
                                    {!! Form::text('input', null, ['class' => 'form-control', 'id' => 'input', 'placeholder' => 'Input']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::checkbox('checkbox', 1, false, ['class' => 'filled-in chk-col-light-blue', 'id' => 'checkbox']) !!}
                                <label for="checkbox">Checkbox de Ejemplo</label>
                            </div>

                            <div class="form-group">
                                {!! Form::radio('radio', 1, false, ['class' => 'with-gap radio-col-light-blue', 'id' => 'radio']) !!}
                                <label for="radio">Radio de Ejemplo</label>
                            </div>

                            <div class="form-group">
                                <label for="fecha">label de fecha</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="mdi mdi-calendar-plus"></i>
                                        </span>
                                    </div>
                                    {!! Form::date('fecha', null, ['class' => 'form-control', 'id' => 'fecha', 'placeholder' => 'dd/mm/aaaa']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::file('input-file-now', ['class' => 'dropify', 'id' => 'input-file-now']) !!}
                            </div>

                            <div class="form-group">
                                <label for="textArea">label de textArea</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="mdi mdi-note-text"></i>
                                        </span>
                                    </div>
                                    {!! Form::textArea('textArea', null, ['class' => 'form-control', 'id' => 'textArea', 'rows' => '3']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password">label de password</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="ti-lock"></i>
                                        </span>
                                    </div>
                                    {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="multiple">label para multiple select</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="mdi mdi-playlist-check"></i>
                                        </span>
                                    </div>
                                    {!! Form::select('multiple', ['1' => 'Uno', '2' => 'Dos', '3' => 'Tres'], null, ['class' => 'form-control custom-select', 'id' => 'selectMul', 'multiple']) !!}
                                </div>
                            </div>

                            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
                            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
         $(document).ready(function() {
            $('.dropify').dropify({
                messages: {
                    default: 'Seleccione o Arrastre el Archivo que Desea Agregar',
                    replace: 'Seleccione o Arrastre un Archivo para Reemplazar',
                    remove: 'Quitar',
                    error: 'Hubo un Error al Tratar de Subir el Archivo'
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#sistemaSel').select2({
                placeholder: 'Seleccione una Opción',
                containerCssClass : "custom-select",
                containerCssClass : "form-control",
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#selectMul').select2({
                placeholder: 'Seleccione Una o Más Opciones',
            });
        });
    </script>
@endpush
