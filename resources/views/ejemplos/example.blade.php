@extends('layouts.app')

@section('content')
<div class="card-body">
    <form class="form-material m-t-40">
        <div class="form-group">
            <label>Default Text</label>
            <input type="text" class="form-control form-control-line" value="Ejemplo"> </div>
        <div class="form-group">
            <label for="example-email">Email</label>
            <input type="email" id="example-email2" name="example-email" class="form-control" placeholder="ejemplo@gmail.com"> </div>
        <div class="form-group">
            <label>Contraseña</label>
            <input type="password" class="form-control" value="password"> </div>
        <div class="form-group">
            <label>Text area</label>
            <textarea class="form-control" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label>File upload</label>
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                <input type="file" name="..."> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="offset-sm-3 col-md-9">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                            <button type="button" class="btn btn-inverse">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection