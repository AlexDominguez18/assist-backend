@extends('layouts.app')

@section('content')

<div class="form-group">
    <div class="ibox-title">
        @if(isset($permiso))
            <h5>Actualizar Permiso</h5>
        @else
            <h5>Nuevo Permiso</h5>
        @endisset
    </div>
    <div class="ibox-content">
        @if(isset($permiso))
            {!! Form::model($permiso, ['route' => ['permisos.update', $permiso->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
        @else
            {!! Form::open(['route' => 'permisos.store'], ['class' => 'form-horizontal']) !!}
        @endif
            <div class="form-group">
                <label for="input">Nombre</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-id-badge"></i>
                        </span>
                    </div>
                    {!! Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'Nombre']) !!}
                </div>
            </div>
            
            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection