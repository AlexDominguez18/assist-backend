@extends('layouts.app')

@section('content')
<div class="row pb-3">
    <div class="col-md-12">
        <a href="{{ route('permisos.create') }}" class="btn btn-sm btn-info">Nuevo Permiso</a>
    </div>
</div>

<div class="row">
    <div class="col-md-8 offset-sm-2">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Permisos</h4>
                <h6 class="card-subtitle">listado de permisos</h6>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($permisos as $permiso)
                                <tr>
                                    <td class="text-center">{{ $permiso->id }}</td>
                                    <td class="text-center">{{ $permiso->nombre }}</td>
                                    <td class="text-nowrap">
                                        {!! Form::open(['route' => ['permisos.destroy', $permiso->id], 'method' => 'delete']) !!}
                                        <a href="{{ route('permisos.edit', $permiso->id) }}" data-toggle="tooltip" data-original-title="Editar"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>
                                        <button type="submit" class="btn" title="Eliminar">
                                        <i class="fas fa-window-close text-danger"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                    
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush