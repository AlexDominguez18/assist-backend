@extends('layouts.app')

@section('content')
<div class="row pb-3">
    <div class="col-md-12">
        <a href="{{ route('roles.create') }}" class="btn btn-sm btn-info">Nuevo Rol</a>
    </div>
</div>
    <div class="row">
        <div class="col-md-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Roles</h4>
                    <h6 class="card-subtitle">Listado de roles</h6>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre del rol</th>
                                    <th scope="col">Sistema Relacionado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $rol)
                                    <tr>
                                        <td class="text-center"><a href="{{ route('roles.show', $rol->id) }}">{{ $rol->nombre }}</a></td>
                                        @foreach($rol->sistema()->withTrashed()->get() as $sistema)
                                            @if($sistema->trashed())
                                                <td>{{ $sistema->nombre }} - Eliminado</td>
                                            @else
                                                <td>{{ $sistema->nombre }}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush