@extends('layouts.app')

@section('content')
<a href="{{ route('roles.index') }}" class="btn btn-sm btn-info">Listado de roles</a>
            <div class="row">
                <div class="col-md-8 offset-sm-2">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Rol</h4>
                            <h6 class="card-subtitle">Descripcion de rol</h6>
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nombre del Rol</th>
                                            <th>Sistema Relacionado</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$rol->nombre}}</td>
                                            <td>{{ $rol->sistema()->get()[0]->nombre }}</td>
                                            <td>
                                                <div style="white-space: nowrap">
                                                    {{-- <a href="{{ route('roles.edit', $rol->id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a> --}}
                                                    <div style="margin-left: 60px">
                                                        {!! Form::open(['route' => ['roles.destroy', $rol->id], 'method' => 'delete']) !!}
                                                        <a href="{{ route('roles.edit', $rol->id) }}" data-toggle="tooltip" data-original-title="Editar"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>
                                                        <button type="submit" class="btn" title="Eliminar">
                                                        <i class="fas fa-window-close text-danger"></i>
                                                        </button>
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@stop

@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush