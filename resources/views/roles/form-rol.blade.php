@extends('layouts.app')

@section('content')

<div class="form-group">
    <div class="ibox-title">
        @if(isset($sistema))
            <h5>Actualizar Rol</h5>
        @else
            <h5>Nuevo Rol</h5>
        @endisset
    </div>
    <div class="ibox-content">
            @if(isset($rol))
                        {!! Form::model($rol, ['route' => ['roles.update', $rol->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
                    @else
                        {!! Form::open(['route' => 'roles.store'], array('class' => 'form-horizontal')) !!}
                    @endif
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-user"></i>
                        </span>
                    </div>
                    {!! Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'Nombre']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-desktop"></i>
                        </span>
                    </div>
                    {!! Form::select('sistema', $sistemas, isset($rol) ? $rol->sistema()->get()[0]->id : null, ['class' => 'form-control', 'id'=> 'sistema']) !!}
                </div>
            </div>

            

            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection