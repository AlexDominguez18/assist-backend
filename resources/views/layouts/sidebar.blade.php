<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="slimScrollDiv">
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav" class="in">
                    <li class="nav-small-cap"></li>
                    @if(!\Auth::check())
                        <li>
                            <a class="waves-effect waves-dark" href="{{ route('login') }}"><i class="mdi mdi-login"></i><span class="hide-menu">Acceder</span></a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="{{ route('register') }}"><i class="mdi mdi-account-plus"></i><span class="hide-menu">Registrarse</span></a>
                        </li>
                    @else
                        <li>
                            <a href="#" onclick="document.getElementById('logout-form').submit();"><i class="mdi mdi-logout"></i><span class="hide-menu">Salir</span></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
