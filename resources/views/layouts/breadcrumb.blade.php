
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles py-2">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor m-l-10 m-b-0 m-t-0">{{ env('APP_NAME') }}</h3>
        <ol class="breadcrumb m-l-10">
            <li class="breadcrumb-active"><a href="{{ route('home') }}">Inicio</a></li>
        </ol>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
