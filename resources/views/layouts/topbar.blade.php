<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <!-- Logo icon -->
                <b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                    <!-- Light Logo icon -->
                    <img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
                    <!-- dark Logo text -->
                    <img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                    <!-- Light Logo text -->
                    <img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
                </span>
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
                        href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item"> <a
                        class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
                        href="javascript:void(0)"><i class="ti-menu"></i></a> </li>

                @if (\Auth::check())
                    <!-- ============================================================== -->
                    <!-- Search -->
                    <!-- ============================================================== -->
                    <li class="nav-item hidden-sm-down search-box">
                        <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark"
                            href="javascript:void(0)"><i class="ti-search"></i></a>
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i
                                    class="ti-close"></i></a>
                        </form>
                    </li>
                @endif
            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">

                @if (\Auth::check())
                    {{-- @include('layouts.notificaciones')
                    --}}
                    <!-- ============================================================== -->
                    <!-- Profile -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img src="https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg" alt="user" width="30"
                                class="profile-pic rounded-circle" />
                        </a>
                        <div class="dropdown-menu mailbox dropdown-menu-right scale-up">
                            <ul class="dropdown-user list-style-none">
                                <li>
                                    <div class="dw-user-box p-3 d-flex">
                                        <div class="u-img"><img src="https://definicion.de/wp-content/uploads/2019/06/perfildeusuario.jpg" alt="user"
                                                class="rounded" width="80"></div>
                                        <div class="u-text ml-2">
                                            <h4 class="mb-0">{{ \Auth::user()->name }}</h4>
                                            <p class="text-muted mb-1 font-14">
                                                {{ \Auth::user()->name }}
                                            </p>
                                            <p class="text-muted mb-1 font-14">
                                                {{ \Auth::user()->email }}
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li role="separator" class="dropdown-divider"></li>
                                <li class="user-list"><a class="px-3 py-2" href="{{ route('profile.show') }}"><i class="ti-user"></i> {{ __('Perfil') }}</a></li>

                                <li role="separator" class="dropdown-divider"></li>

                                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                                    <li class="user-list"><a class="px-3 py-2" href="{{ route('api-tokens.index') }}"><i
                                                class="ti-settings"></i> {{ __('API Tokens') }}</a></li>
                                @endif

                                @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                                    <li class="user-list">
                                        <a class="px-3 py-2" href="{{ route('teams.show', Auth::user()->currentTeam->id) }}">{{ __('Team Settings') }}</a>
                                    </li>

                                    <!-- Team Settings -->
                                    @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                                        <li class="user-list">
                                            <a href="{{ route('teams.create') }}" class="px-3 py-2">{{ __('Create New Team') }}</a>
                                        </li>
                                    @endcan
                                @endif

                                <li role="separator" class="dropdown-divider"></li>

                                <li class="user-list">
                                    <a class="px-3 py-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out-alt"></i> {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>

                @endif
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
