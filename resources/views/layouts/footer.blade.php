<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer d-flex justify-content-between" style="<!-- padding: .5em 15px; -->">
    <div>
        Universidad de Guadalajara &copy; 2020
    </div>
    <div>
    	<strong>{{ env('CENTRO_UNIVERSITARIO', 'CUCEI') }}</strong>
    </div>
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
