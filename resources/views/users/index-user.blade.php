@extends('layouts.app')

@section('content')
<div class="row pb-3">
    <div class="col-md-12">
        <a href="{{ route('users.create') }}" class="btn btn-sm btn-info">Nuevo Usuario</a>
    </div>
</div>
    <div class="row">
        <div class="col-md-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Usuarios</h4>
                    <h6 class="card-subtitle">listado de usuarios</h6>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Cargo</th>
                                    <th scope="col">Permiso</th>
                                    <th scope="col">Correo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td class="text-center"><a
                                                href="{{ route('users.show', $user->id) }}">{{ $user->name }}</a>
                                        </td>
                                        <td>{{ $user->cargo }}</td>
                                        <td>
                                            @foreach ($user->permisos()->get() as $permiso)
                                                <li>
                                                    {{ $permiso->nombre }}
                                                </li>
                                            @endforeach
                                        </td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                @endforeach
                            </tbody>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush
