@extends('layouts.app')

@section('content')
<div class="row pb-3">
    <div class="col-md-12">
        <a href="{{ route('users.index') }}" class="btn btn-sm btn-info">Listado de Usuarios</a>
    </div>
</div>
    <div class="row">
        <div class="col-md-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Usuario</h4>
                    <h6 class="card-subtitle">detalle de usuario</h6>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Cargo</th>
                                    <th scope="col">Permiso</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td class="text-center">
                                            {{ $user->name }}
                                        </td>
                                        <td>{{ $user->cargo }}</td>
                                        <td>
                                            @foreach ($user->permisos()->get() as $permiso)
                                                <li>
                                                    {{ $permiso->nombre }}
                                                </li>
                                            @endforeach
                                        </td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            <div style="white-space: nowrap">
                                                {{-- <a href="{{ route('roles.edit', $rol->id) }}" data-toggle="tooltip" data-original-title="Edit"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a> --}}
                                                <div style="margin-left: 60px">
                                                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                                                    <a href="{{ route('users.edit', $user->id) }}" data-toggle="tooltip" data-original-title="Editar"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>
                                                    <button type="submit" class="btn" title="Eliminar">
                                                    <i class="fas fa-window-close text-danger"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                            </tbody>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush
