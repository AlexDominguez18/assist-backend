@extends('layouts.app')

@section('content')

<div class="form-group">
    <div class="ibox-title">
        @if(isset($user))
            <h5>Actualizar Usuario</h5>
        @else
            <h5>Nuevo Usuario</h5>
        @endisset
    </div>
    <div class="ibox-content">
        @if(isset($user))
            {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
        @else
            {!! Form::open(['route' => 'users.store'], ['class' => 'form-horizontal']) !!}
        @endif
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('name', 'Nombre', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-user"></i>
                        </span>
                    </div>
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nombre']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('cargo', 'Cargo', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-stamp"></i>
                        </span>
                    </div>
                    {!! Form::text('cargo', null, ['class' => 'form-control', 'id' => 'cargo', 'placeholder' => 'Cargo']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('permisos', 'Permisos', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-id-badge"></i>
                        </span>
                    </div>
                    {!! Form::select('permisos[]', $Permisos, null, ['class' => 'form-control', 'id'=> 'permisos', 'required', 'multiple']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('email', 'Correo', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-email"></i>
                        </span>
                    </div>
                    {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Correo']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('password', 'Contraseña', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-lock"></i>
                        </span>
                    </div>
                    {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::label('password', 'Confirmar Contraseña', ['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-lock"></i>
                        </span>
                    </div>
                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation']) !!}
                </div>
            </div>
            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#permisos').select2({
            placeholder: 'Seleccione Una o Más Permisos',
        });
    });
</script>
@endpush
