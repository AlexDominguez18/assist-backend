@extends('layouts.app')

@section('content')

<div class="form-group">
    <div class="ibox-title">
        @if(isset($actualizacion))
            <h5>Actualizar Actualizacion</h5>
        @else
            <h5>Nueva Actualizacion</h5>
        @endisset
    </div>
    <div class="ibox-content">
            @if(isset($actualizacion))
                        {!! Form::model($actualizacion, ['route' => ['actualizaciones.update', $actualizacion->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}
                    @else
                        {!! Form::open(['route' => 'actualizaciones.store'], array('class' => 'form-horizontal')) !!}
                    @endif
            <div class="form-group">
                <label for="titulo">Titulo</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-text"></i>
                        </span>
                    </div>
                    {!! Form::text('titulo', null, ['class' => 'form-control', 'id' => 'titulo']) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="descripcion">Descripcion</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="mdi mdi-note-text"></i>
                        </span>
                    </div>
                    {!! Form::textArea('descripcion', null, ['class' => 'form-control', 'id' => 'descripcion', 'rows' => '3']) !!}
                </div>
            </div>
            
            <div class="form-group">
                <label for="rol">Seleccionar rol</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-user"></i>
                        </span>
                    </div>
                    {!! Form::select('roles[]', $roles ?? $Roles,  null, ['class' => 'form-control', 'id'=> 'rol']) !!}
                </div>
            </div>

            <div class="form-group">
                <label for="sistema">Seleccionar sistema</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="ti-desktop"></i>
                        </span>
                    </div>
                    {!! Form::select('sistemas[]', $sistemas ?? $Sistemas,  null, ['class' => 'form-control', 'id'=> 'sistema']) !!}
                </div>
            </div>

            

            {!! Form::button('Aceptar', ['class' => 'btn btn-info waves-effect waves-light m-r-10', 'type' => 'submit']) !!}
            {!! Form::button('Cancelar', ['class' => 'btn btn-inverse waves-effect waves-light', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>

</div>
@endsection