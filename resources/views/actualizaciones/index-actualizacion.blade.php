@extends('layouts.app')

@section('content')
<div class="row pb-3">
    <div class="col-md-12">
        <a href="{{ route('actualizaciones.create') }}" class="btn btn-sm btn-info">Nueva Actualizacion</a>
    </div>
</div>

    <div class="row">
        <div class="col-md-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Actualizaciones</h4>
                    <h6 class="card-subtitle">Listado Actualizaciones</h6>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Titulo</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Rol</th>
                                    <th scope="col">Sistemas</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($actualizaciones as $actualizacion)
                                    <tr>
                                        <td class="text-center"><a href="{{ route('actualizaciones.show', $actualizacion->id) }}">{{ $actualizacion->titulo }}</a></td>
                                        <td>{{ $actualizacion->descripcion }}</td>
                                        <td>
                                            <ul>
                                                @foreach ($actualizacion->roles()->get() as $rol)
                                                    <li>
                                                        @if($rol->trashed())
                                                            {{ $rol->NombreRolNombreSistema}} - Eliminado
                                                        @else
                                                            {{ $rol->NombreRolNombreSistema}}
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>
                                            <ul>
                                                @foreach ($actualizacion->sistemas()->get() as $sistema)
                                                    <li>
                                                        @if($sistema->trashed())
                                                            {{ $sistema->nombre}} - Eliminado
                                                        @else
                                                            {{ $sistema->nombre}}
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush