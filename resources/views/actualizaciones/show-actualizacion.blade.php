@extends('layouts.app')

@section('content')
<a href="{{ route('actualizaciones.index') }}" class="btn btn-sm btn-info">Listado de actualizaciones</a>
            <div class="row">
                <div class="col-md-8 offset-sm-2">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Actualizaciones</h4>
                            <h6 class="card-subtitle">Descripcion de actualizacion</h6>
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Titulo</th>
                                            <th scope="col">Descripcion</th>
                                            <th scope="col">Rol</th>
                                            <th scope="col">Sistemas</th>
                                            <th scope="col">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td class="text-center"><a href="{{ route('actualizaciones.show', $actualizacion->id) }}">{{ $actualizacion->titulo }}</a></td>
                                                <td>{{ $actualizacion->descripcion }}</td>
                                                <td>
                                                    <ul>
                                                        @foreach ($actualizacion->roles()->get() as $rol)
                                                            <li>
                                                                @if($rol->trashed())
                                                                    {{ $rol->NombreRolNombreSistema}} - Eliminado
                                                                @else
                                                                    {{ $rol->NombreRolNombreSistema}}
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($actualizacion->sistemas()->get() as $sistema)
                                                            <li>
                                                                @if($sistema->trashed())
                                                                    {{ $sistema->nombre}} - Eliminado
                                                                @else
                                                                    {{ $sistema->nombre}}
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                        {!! Form::open(['route' => ['actualizaciones.destroy', $actualizacion->id], 'method' => 'delete']) !!}
                                                        <a href="{{ route('actualizaciones.edit', $actualizacion->id) }}" data-toggle="tooltip" data-original-title="Editar"> <i class="fas fa-pencil-alt text-inverse m-r-10"></i> </a>
                                                        <button type="submit" class="btn" title="Eliminar">
                                                        <i class="fas fa-window-close text-danger"></i>
                                                        </button>
                                                        {!! Form::close() !!}
                                                </td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@stop

@push('scripts')
    <script>
        $(function () {
            $('#myTable').DataTable();
        });
    </script>
@endpush