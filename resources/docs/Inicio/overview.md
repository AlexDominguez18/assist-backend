# Inicio

---

- [¿Qué es?](#que-es)

<a name="que-es"></a>
## ¿Qué es {{ env('APP_NAME') }}?

{{ env('APP_NAME') }} es una aplicación web creada para dar soporte a otros sistemas de la Universidad de Guadalajara.