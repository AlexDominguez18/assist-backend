# Login

- [Ruta](#ruta)
- [Argumentos](#argumentos)
- [Respuestas](#respuestas)

<a name="ruta"></a>
## Ruta
---
La ruta de la API para que un usuario pueda  `Logearse/Iniciar sesión` es la siguiente:

-  ```{{env('APP_NAME')}}/api/login```

<a name="argumentos"></a>
## Argumentos
---
Los argumentos necesarios para el funcionamiento de esta ruta son:

- ```email```
- ```password```

<a name="respuestas"></a>
## Respuestas
---
Existen 2 posibles respuestas:

- `401` - Si las credenciales no coinciden con los registros de la base de datos.

- `200` - En el caso de que los parámetros eran correctos y las credenciales correspondan con los registros de la base de datos.
    En este caso la respuesta del servidor viene acompañada de los siguientes valores:
    - `access_token`
    - `type_token`