# Register

- [Ruta](#ruta)
- [Argumentos](#argumentos)
- [Respuestas](#respuestas)

<a name="ruta"></a>
## Ruta
---
La ruta de la API para dar de alta o registar a un usuario es la siguiente:

- ```{{env('APP_URL')}}/api/register```

<a name="argumentos"></a>
## Argumentos
---
Los argumentos necesarios para el funcionamiento de esta ruta son:

- ```name```
- ```email```
- ```password```
- ```password_confirmation```

<a name="respuestas"></a>
## Respuestas
---
Existen dos posibles respuestas:

- `422` - En el caso de que alguno de los parámetros no cumpla con las reglas de validación correspondientes.

- `200` - En el caso de que los parámetros eran correctos y el registro se llevó a cabo de manera exitosa.
    En este caso la respuesta del servidor viene acompañada de los siguientes valores:
    - `access_token`
    - `type_token`