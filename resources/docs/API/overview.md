# {{env('APP_NAME')}} API

- [Authentication](#authentication)

Este sistema está construido con `Laravel` en el backend por lo que el manejo de la información se realiza mediante el consumo de las rutas API que se proporcionan. Las rutas de la API se clasifican según el recurso o la acción que se maneje, a continuación se enlistan todas las rutas:

<a name="authentication"></a>
## Authentication

- [`Register`](/{{route}}/{{version}}/auth/register)
- [`Login`](/{{route}}/{{version}}/auth/login)