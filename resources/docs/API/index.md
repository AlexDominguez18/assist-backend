- ## API
    - [Información](/{{route}}/{{version}}/overview)

- ## Authentication
    - [Register](/{{route}}/{{version}}/auth/register)
    - [LogIn](/{{route}}/{{version}}/auth/login)